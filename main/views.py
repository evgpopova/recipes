from django.views import generic
from .models import Recipe, Writer
from .forms import RecipeForm, SignUpForm
from django.shortcuts import redirect
from guardian.shortcuts import assign_perm


class IndexView(generic.ListView):
    template_name = 'main/index.html'
    context_object_name = 'recipes_list'

    def get_queryset(self, **kwargs):
        filter_val = self.kwargs.get('filter')
        if filter_val:
            if filter_val == 'all':
                new_context = Recipe.objects.all()
            else:
                new_context = Recipe.objects.filter(meal=filter_val)
        else:
            new_context = Recipe.objects.all()
        return new_context


class RecipeUpdate(generic.UpdateView):
    model = Recipe
    form_class = RecipeForm

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        user = self.request.user
        if user.has_perm('edit_recipe', obj):
            return super(RecipeUpdate, self).dispatch(request, *args, **kwargs)
        else:
            return redirect(obj)


class RecipeDetail(generic.DetailView):
    model = Recipe


class RecipeCreate(generic.CreateView):
    model = Recipe
    form_class = RecipeForm

    def form_valid(self, form):
        recipe = form.save(commit=False)
        user = self.request.user
        recipe.writer = Writer.objects.get(user=user.id)
        recipe.save()
        assign_perm('edit_recipe', user, recipe)
        print(user.has_perm('edit_recipe', recipe))
        return super().form_valid(form)


class SignUpView(generic.FormView):
    template_name = 'main/signup.html'
    form_class = SignUpForm
    success_url = '/login'

    def form_valid(self, form):
        user = form.save()
        return super(SignUpView, self).form_valid(form)