$(document).ready(function() {
    $('#id_ingredient').select2({
        multiple: true,
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === '' || /^\d+$/.test(term)) {
              return null;
            }
            return {
                id: term,
                text: term,
                newTag: true
            }
        }
    });
    $('#id_meal').select2({
        multiple: false
    });
});
