from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Ingredient(models.Model):
    name = models.CharField('Название', max_length=20)

    def __str__(self):
        return self.name


class Writer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Recipe(models.Model):
    MEAL_CHOICES = (('all', 'ALL'),('breakfast', 'Breakfasts'),
                    ('main-dish', 'Main Dishes'), ('desserts', 'Desserts'),
                    ('salads', 'Salads'), ('soups', 'Soups'))
    name = models.CharField('Name', max_length=200)
    description = models.TextField('Description', max_length=2000)
    meal = models.CharField('Meal', choices=MEAL_CHOICES, blank=True,
                            max_length=10)
    pub_date = models.DateField(auto_now_add=True)
    writer = models.ForeignKey(Writer, on_delete=models.CASCADE, blank=True,
                               null=True)
    ingredient = models.ManyToManyField(Ingredient)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('main:get_recipe', kwargs={'pk': self.pk})

    class Meta:
        permissions = (
            ('edit_recipe', 'Edit recipe'),
        )


@receiver(post_save, sender=User)
def update_user_writer(sender, instance, created, **kwargs):
    if created:
        Writer.objects.create(user=instance)
    instance.writer.save()