from .models import Recipe


def meals_processor(request):
    meals = Recipe.MEAL_CHOICES
    return {'meals': meals}