from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from .models import Recipe, Ingredient


class RecipeForm(forms.ModelForm):
    meal = forms.ChoiceField(required=True, choices=Recipe.MEAL_CHOICES)

    class Meta:
        model = Recipe
        exclude = ['pub_date', 'writer']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'ingredient' in self.data:
            ingredients = self.data.getlist('ingredient')
            list_of_ingredient_ids = []
            for ingredient in ingredients:
                if ingredient.isdigit():
                    list_of_ingredient_ids.append(ingredient)
                else:
                    new_ingredient = Ingredient.objects.create(name=ingredient)
                    list_of_ingredient_ids.append(str(new_ingredient.id))
                self.ingredients = kwargs.pop('url', list_of_ingredient_ids)
            super(RecipeForm, self).__init__(*args, **kwargs)

    def is_valid(self):
        valid = super(RecipeForm, self).is_valid()
        if self.errors.get("ingredient"):
            if len(Ingredient.objects.filter(id__in = self.ingredients)) == \
                    len(self.ingredients):
                del self.errors["ingredient"]
        if len(self.errors) == 0:
            self.cleaned_data["ingredient"] = Ingredient.objects.filter(id__in = self.ingredients)
            return True
        else:
            return False


class IngredientForm():
    class Meta:
        model = Ingredient


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    password = forms.CharField(label="Password", widget=forms.PasswordInput(
        attrs={'class': 'form-control'}))


class SignUpForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', )