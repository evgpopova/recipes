from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
from .forms import LoginForm


app_name = 'main'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^filter/(?P<filter>[-\w]+)/$', views.IndexView.as_view(), name='filtered-recipes'),
    url(r'^(?P<pk>\d+)/$', views.RecipeDetail.as_view(), name='get_recipe'),
    url(r'^edit/(?P<pk>\d+)/$', views.RecipeUpdate.as_view(), name='edit_recipe'),
    url(r'^add', views.RecipeCreate.as_view(), name='add_recipe'),
    url(r'^login/$', auth_views.LoginView.as_view(form_class = LoginForm,
                                                 template_name = "main/login.html"),name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^signup/$', views.SignUpView.as_view(), name='signup')
]