# Recipes Django Project

This is a test application based on Django framework where users can create and
edit their own recipes. Demo version is available here [Recipes](https://recipes-django.herokuapp.com)

### Prerequisites

pip
python 
postgres